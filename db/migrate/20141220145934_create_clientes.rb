class CreateClientes < ActiveRecord::Migration
  def change
    create_table :clientes do |t|
      t.string :nome
      t.string :sobrenome
      t.string :email
      t.string :telefone
      t.string :celular
      t.string :cpf
      t.string :senha_digest

      t.timestamps
    end
  end
end
