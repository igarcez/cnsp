class ConcertaColunaSenhaCliente < ActiveRecord::Migration
  def change
    rename_column :clientes, :senha_digest, :password_digest
  end
end
