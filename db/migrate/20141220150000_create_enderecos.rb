class CreateEnderecos < ActiveRecord::Migration
  def change
    create_table :enderecos do |t|
      t.integer :cep
      t.string :rua
      t.string :bairro
      t.string :numero
      t.string :complemento
      t.string :cidade
      t.string :estado
      t.references :cliente, index: true

      t.timestamps
    end
  end
end
