class Cliente < ActiveRecord::Base
  has_secure_password

  validates :nome, :presence => true
  validates :sobrenome, :presence => true
  validates :email, :presence => true, uniqueness: {message: "Usu&aacute;rio j&aacute; cadastrado", case_sensitive: false}

  has_many :enderecos

  def nome_simples
    "#{nome}"
  end

  def nome_completo
    "#{nome} #{sobrenome}"
  end

  def reset_password
    self.password = SecureRandom.hex(8)
    self.password_confirmation = self.password
    self.save!
    #todo mandar email para cliente
    self.password
  end

  def self.authenticate(email, password)
    user = self.where(:email => email).first
    return false if user.nil?
    return false unless user.authenticate(password)
    user
  end
end
