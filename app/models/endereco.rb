class Endereco < ActiveRecord::Base
  belongs_to :cliente

  validates :rua, :presence => true
  validates :numero, :presence => true
  validates :cep, :presence => true

end
