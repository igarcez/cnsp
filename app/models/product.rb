class Product < Shoppe::Product
  def path
    "produtos/#{self.product_category.permalink}/#{self.permalink}"
  end
end