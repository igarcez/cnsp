class PagesController < ApplicationController
  def home
    @products = Shoppe::Product.active.includes(:default_image, :product_category, :variants).root

    @clientes = Cliente.all
  end
end
