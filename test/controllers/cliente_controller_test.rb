require 'test_helper'

class ClienteControllerTest < ActionController::TestCase
  def test_cliente_nome_completo
    cliente = Cliente.new(nome: "teste", sobrenome: "de teste")
    assert_equal 'teste de teste', cliente.nome_completo
  end

  def test_um_email_por_cliente
    cliente1 = Cliente.new(nome: "teste", sobrenome: "de teste", email: "teste@teste.com.br", password: "teste", password_confirmation: "teste");
    cliente2 = Cliente.new(nome: "teste 2", sobrenome: "de teste", email: "teste@teste.com.br", password: "teste", password_confirmation: "teste");
    assert cliente1.valid?
    assert cliente2.valid?
    assert cliente1.save
    assert_not cliente2.save
  end
end
