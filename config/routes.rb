Rails.application.routes.draw do
  mount Shoppe::Engine => "/shoppe"

  root :to => 'pages#home'

  get 'procura' => 'search#index', :as => 'procura'

  #
  # Product browising
  #
  get 'produtos' => 'products#categories', :as => 'catalogue'
  get 'produtos/filter' => 'products#filter', :as => 'product_filter'
  get 'produtos/:category_id' => 'products#index', :as => 'products'
  get 'produtos/:category_id/:product_id' => 'products#show', :as => 'product'
  post 'produtos/:category_id/:product_id/buy' => 'products#add_to_basket', :as => 'buy_product'
end
